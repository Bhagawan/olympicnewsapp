package com.example.sportnews.util

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class OlympicResult(@SerializedName("name") val country : String,
                         @SerializedName("flag") val img : String, val gold : Int, val silver : Int,
                         val bronse : Int)
