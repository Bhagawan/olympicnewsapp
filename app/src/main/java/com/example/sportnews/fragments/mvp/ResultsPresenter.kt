package com.example.sportnews.fragments.mvp

import com.example.sportnews.util.OlympicResult
import com.example.sportnews.util.ServerClient
import moxy.InjectViewState
import moxy.MvpPresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@InjectViewState
class ResultsPresenter : MvpPresenter<ResultsPresenterViewInterface>() {

    override fun onFirstViewAttach() {
        ServerClient.create().getResults().enqueue(object: Callback<List<OlympicResult>> {
            override fun onResponse(call: Call<List<OlympicResult>>, response: Response<List<OlympicResult>>) {
                if(response.isSuccessful) response.body()?.let { viewState.fillResults(it) }
                else viewState.serverError()
            }

            override fun onFailure(call: Call<List<OlympicResult>>, t: Throwable) {
                viewState.serverError()
            }

        })
    }
}