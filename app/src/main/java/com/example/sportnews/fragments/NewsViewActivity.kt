package com.example.sportnews.fragments

import android.os.Bundle
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.example.sportnews.R
import com.example.sportnews.util.News
import com.squareup.picasso.Picasso

class NewsViewActivity : AppCompatActivity() {

    override fun onBackPressed() {
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_view)
        val b = intent.getSerializableExtra("news") as News
        fillNews(b)
        val mToolBar : Toolbar = findViewById(R.id.toolbar_news_view)
        setSupportActionBar(mToolBar)
        mToolBar.setBackgroundColor(ContextCompat.getColor(this, R.color.toolbar))
        mToolBar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24)
        supportActionBar?.title = ""
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> finish()
        }
        return true
    }


    private fun fillNews(news : News) {
        val header : TextView = findViewById(R.id.textview_news_view_header)
        val img : ImageView = findViewById(R.id.textview_news_view_img)
        val text : TextView = findViewById(R.id.textview_news_view_text)
        header.text = news.header
        Picasso.get().load(news.img).into(img)
        text.text = news.text
    }
}