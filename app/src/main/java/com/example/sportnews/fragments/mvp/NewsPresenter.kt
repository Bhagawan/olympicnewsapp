package com.example.sportnews.fragments.mvp

import com.example.sportnews.util.News
import com.example.sportnews.util.ServerClient
import moxy.InjectViewState
import moxy.MvpPresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@InjectViewState
class NewsPresenter : MvpPresenter<NewsPresenterViewInterface>() {

    private val serverClient by lazy {
        ServerClient.create()
    }

    override fun onFirstViewAttach() {
        serverClient.getNews().enqueue(object: Callback<List<News>> {
            override fun onResponse(call: Call<List<News>>, response: Response<List<News>>) {
                if(response.isSuccessful) response.body()?.let { viewState.fillNews(it) }
                else viewState.serverError()
            }

            override fun onFailure(call: Call<List<News>>, t: Throwable) {
                viewState.serverError()
            }

        })
    }
}