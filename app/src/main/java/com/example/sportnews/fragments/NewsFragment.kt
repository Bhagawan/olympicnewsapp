package com.example.sportnews.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sportnews.R
import com.example.sportnews.fragments.adapter.NewsAdapter
import com.example.sportnews.fragments.mvp.NewsPresenter
import com.example.sportnews.fragments.mvp.NewsPresenterViewInterface
import com.example.sportnews.util.News
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter

class NewsFragment : MvpAppCompatFragment(), NewsPresenterViewInterface {

    private lateinit var mView: View

    @InjectPresenter
    lateinit var mPresenter: NewsPresenter

    override fun onCreateView( inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        mView = inflater.inflate(R.layout.fragment_news, container, false)
        return mView
    }

    override fun fillNews(news: List<News>) {
        val recycler : RecyclerView = mView.findViewById(R.id.recycler_news)
        recycler.layoutManager = LinearLayoutManager(context)
        val newsAdapter = NewsAdapter(news)
        newsAdapter.setCallBack(object :NewsAdapter.TouchCallback {
            override fun onTouch(position: Int) {
                val intent = Intent(context, NewsViewActivity::class.java)
                intent.putExtra("news", news[position])
                startActivity(intent)
            }
        })
        recycler.adapter = newsAdapter

    }

    override fun serverError() = Toast.makeText(context, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show()
}