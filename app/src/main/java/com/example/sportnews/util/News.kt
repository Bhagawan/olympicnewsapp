package com.example.sportnews.util

import androidx.annotation.Keep
import java.io.Serializable

@Keep
data class News(val header: String, val img: String, val text: String) :Serializable
