package com.example.sportnews.fragments.mvp

import com.example.sportnews.util.News
import moxy.MvpView
import moxy.viewstate.strategy.alias.SingleState

interface NewsPresenterViewInterface : MvpView {

    @SingleState
    fun fillNews(news :List<News>)

    @SingleState
    fun serverError()
}