package com.example.sportnews.fragments.mvp

import moxy.MvpView
import moxy.viewstate.strategy.alias.OneExecution
import moxy.viewstate.strategy.alias.SingleState

interface SplashPresenterViewInterface : MvpView {
    @SingleState
    fun switchToMain()

    @OneExecution
    fun showError(error: String?)

    @SingleState
    fun showSplash(url: String?)

    @OneExecution
    fun showLogo()

    @OneExecution
    fun hideLogo()
}