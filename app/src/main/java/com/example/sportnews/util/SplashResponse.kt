package com.example.sportnews.util

import androidx.annotation.Keep

@Keep
data class SplashResponse(val url : String)
