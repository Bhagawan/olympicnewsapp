package com.example.sportnews.fragments.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.sportnews.R
import com.example.sportnews.util.News
import com.squareup.picasso.Picasso

class NewsAdapter(private val news: List<News>) : RecyclerView.Adapter<NewsAdapter.ViewHolder>() {
    private lateinit var callback: TouchCallback

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_news, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.header.text = news[position].header
        Picasso.get().load(news[position].img).into(holder.img)
        val t = news[position].text.take(100) + "..."
        holder.text.text = t
        holder.itemView.setOnClickListener {
            callback.onTouch(position)
        }
    }

    override fun getItemCount(): Int {
        return news.size
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val header: TextView = itemView.findViewById(R.id.news_card_header)
        val img : ImageView = itemView.findViewById(R.id.news_card_image)
        val text : TextView = itemView.findViewById(R.id.news_card_text)
    }

    fun setCallBack(callback: TouchCallback) {
        this.callback = callback
    }

    interface TouchCallback {
        fun onTouch(position : Int)
    }
}