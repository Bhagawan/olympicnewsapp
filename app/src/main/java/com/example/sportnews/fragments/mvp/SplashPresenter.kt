package com.example.sportnews.fragments.mvp

import com.example.sportnews.util.ServerClient
import com.example.sportnews.util.SplashResponse
import moxy.InjectViewState
import moxy.MvpPresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

@InjectViewState
class SplashPresenter : MvpPresenter<SplashPresenterViewInterface>() {
    private var requestInProcess = false

    fun requestSplash() {
        val call :Call<SplashResponse> = ServerClient.create().getSplash(Locale.getDefault().language)
        showLogo()
        call.enqueue(object : Callback<SplashResponse> {
            override fun onResponse(
                call: Call<SplashResponse>,
                response: Response<SplashResponse>
            ) {
                if (requestInProcess) {
                    requestInProcess = false
                    viewState.hideLogo()
                }
                if (response.isSuccessful && response.body() != null) {
                    val s: String = response.body()!!.url
                    if (s == "no") viewState.switchToMain()
                    else viewState.showSplash("https://$s")
                } else {
                    viewState.showError("Server error 2")
                    viewState.switchToMain()
                }
            }

            override fun onFailure(call: Call<SplashResponse>, t: Throwable) {
                viewState.showError("Server error 3")
                viewState.switchToMain()
            }
        })
    }

    private fun showLogo() {
        viewState.showLogo()
        requestInProcess = true
    }
}