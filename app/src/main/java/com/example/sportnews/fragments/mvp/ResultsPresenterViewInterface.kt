package com.example.sportnews.fragments.mvp

import com.example.sportnews.util.OlympicResult
import moxy.MvpView
import moxy.viewstate.strategy.alias.SingleState

interface ResultsPresenterViewInterface : MvpView {

    @SingleState
    fun fillResults(results :List<OlympicResult>)

    @SingleState
    fun serverError()
}