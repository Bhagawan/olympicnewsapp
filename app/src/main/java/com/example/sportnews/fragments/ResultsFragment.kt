package com.example.sportnews.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sportnews.R
import com.example.sportnews.fragments.adapter.ResultsAdapter
import com.example.sportnews.fragments.mvp.ResultsPresenter
import com.example.sportnews.fragments.mvp.ResultsPresenterViewInterface
import com.example.sportnews.util.OlympicResult
import moxy.MvpAppCompatFragment
import moxy.presenter.InjectPresenter


class ResultsFragment : MvpAppCompatFragment(), ResultsPresenterViewInterface {
    private lateinit var mView: View

    @InjectPresenter
    lateinit var mPresenter: ResultsPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mView = inflater.inflate(R.layout.fragment_results, container, false)
        return mView
    }

    override fun fillResults(results: List<OlympicResult>) {
        val recycler : RecyclerView = mView.findViewById(R.id.recycler_results)
        recycler.layoutManager = LinearLayoutManager(context)
        recycler.adapter = ResultsAdapter(results)
    }

    override fun serverError() = Toast.makeText(context, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show()
}