package com.example.sportnews.fragments.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.sportnews.R
import com.example.sportnews.util.OlympicResult
import com.squareup.picasso.Picasso

class ResultsAdapter (private val results: List<OlympicResult>?) : RecyclerView.Adapter<ResultsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_results, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if(position == 0) {
            holder.place.text = ""
            holder.flag.setImageBitmap(null)
            holder.country.text = holder.itemView.context.getString(R.string.msg_result_country)
            holder.gold.text = holder.itemView.context.getString(R.string.msg_result_gold)
            holder.silver.text = holder.itemView.context.getString(R.string.msg_result_silver)
            holder.bronze.text = holder.itemView.context.getString(R.string.msg_result_bronze)
            holder.total.text = holder.itemView.context.getString(R.string.msg_result_total)
        } else if(results != null){
            holder.place.text = position.toString()
            Picasso.get().load(results[position - 1].img).into(holder.flag)
            holder.country.text = results[position - 1].country
            holder.gold.text = results[position - 1].gold.toString()
            holder.silver.text = results[position - 1].silver.toString()
            holder.bronze.text = results[position - 1].bronse.toString()
            val tot =  results[position - 1].gold + results[position - 1].silver + results[position - 1].bronse
            holder.total.text = tot.toString()
        }
    }

    override fun getItemCount(): Int {
        return results?.size?.plus(1) ?: 1
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val place: TextView = itemView.findViewById(R.id.item_result_place)
        val flag : ImageView = itemView.findViewById(R.id.item_result_img)
        val country : TextView = itemView.findViewById(R.id.item_result_name)
        val gold : TextView = itemView.findViewById(R.id.item_result_gold)
        val silver : TextView = itemView.findViewById(R.id.item_result_silver)
        val bronze : TextView = itemView.findViewById(R.id.item_result_bronse)
        val total : TextView = itemView.findViewById(R.id.item_result_total)
    }
}